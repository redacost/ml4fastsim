import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras import backend as K

from core.constants import BATCH_SIZE_PER_REPLICA, N_CELLS_R, N_CELLS_PHI, N_CELLS_Z, SINKHORN_NUM_ITERATES

from tensorflow.keras.losses import BinaryCrossentropy, Reduction, MeanSquaredError


class _Sampling(layers.Layer):
    """ Custom layer to do the reparameterization trick: sample random latent vectors z from the latent Gaussian
    distribution.

    The sampled vector z is given by sampled_z = mean + std * epsilon
    """

    def __call__(self, inputs, **kwargs):
        z_mean, z_log_var, epsilon = inputs
        z_sigma = K.exp(0.5 * z_log_var)
        return z_mean + z_sigma * epsilon


# KL divergence computation
class _KLDivergenceLayer(layers.Layer):

    def call(self, inputs, **kwargs):
        mu, log_var = inputs
        kl_loss = -0.5 * (1 + log_var - K.square(mu) - K.exp(log_var))
        kl_loss = K.mean(K.sum(kl_loss, axis=-1))
        self.add_loss(kl_loss)
        return inputs

class _SinkHornLayer(layers.Layer):
    def call(self, inputs, latent_dim, noise = None, cond_x = None, **kwargs):
        epsilon = 1e-8
        bs = tf.shape(inputs)[0]
        if noise == None:
            y_noise = tf.random.uniform((bs,latent_dim),minval=-1.0,maxval=1.0)
        else:
            y_noise = noise
        if cond_x == None:
            sh_loss = sinkhorn_loss(inputs,y_noise,epsilon,bs,SINKHORN_NUM_ITERATES)
        else:
            sh_loss = sinkhorn_loss(tf.concat([inputs,cond_x],axis=1),tf.concat([y_noise,cond_x],axis=1),epsilon,bs,SINKHORN_NUM_ITERATES)
        self.add_loss(sh_loss)
        return inputs
        


# Physics observables
def _PhysicsLosses(y_true, y_pred):
    y_true = tf.reshape(y_true, (-1, N_CELLS_R, N_CELLS_PHI, N_CELLS_Z))
    y_pred = tf.reshape(y_pred, (-1, N_CELLS_R, N_CELLS_PHI, N_CELLS_Z))
    # longitudinal profile
    longitudinal_loss = MeanSquaredError(reduction=Reduction.SUM)(tf.reduce_sum(y_true, axis=(1, 2)), tf.reduce_sum(y_pred, axis=(1, 2)))# / (BATCH_SIZE_PER_REPLICA * N_CELLS_R * N_CELLS_PHI)
    #tf.print(longitudinal_loss)
    # lateral profile
    lateral_loss = MeanSquaredError(reduction=Reduction.SUM)(tf.reduce_sum(y_true, axis=(2, 3)), tf.reduce_sum(y_pred, axis=(2, 3)))# / (BATCH_SIZE_PER_REPLICA * N_CELLS_Z * N_CELLS_PHI)
    #tf.print(lateral_loss)
    # full energy
    full_loss = MeanSquaredError(reduction=Reduction.SUM)(tf.reduce_sum(y_true, axis=(1, 2, 3)), tf.reduce_sum(y_pred, axis=(1, 2, 3)))# / (BATCH_SIZE_PER_REPLICA * N_CELLS_R * N_CELLS_Z * N_CELLS_PHI)
    #tf.print(full_loss)
    loss = full_loss #longitudinal_loss + lateral_loss + full_loss
    return loss


def TransformerEncoderBlock(inputs, num_heads, projection_dim, ff_dims, dropout=0.1):
    # Layer normalization 1.
    x1 = layers.LayerNormalization(epsilon=1e-6)(inputs)
    # Create a multi-head attention layer.
    attention_output = layers.MultiHeadAttention(
        num_heads=num_heads, key_dim=projection_dim, dropout=dropout)(x1, x1)
    # Skip connection 1.
    x2 = layers.Add()([attention_output, inputs])
    # Layer normalization 2.
    x3 = layers.LayerNormalization(epsilon=1e-6)(x2)
    # MLP.
    x3 = mlp(x3, hidden_units=ff_dims, dropout_rate=dropout)
    # Skip connection 2.
    return layers.Add()([x3, x2])


def mlp(x, hidden_units, dropout_rate):
    for units in hidden_units:
        x = layers.Dense(units, activation=tf.nn.gelu)(x)
        x = layers.Dropout(dropout_rate)(x)
    return x

def cost_matrix(x,y,p=2):
    "Returns the cost matrix C_{ij}=|x_i - y_j|^p"
    x_col = tf.expand_dims(x,1)
    y_lin = tf.expand_dims(y,0)
    c = tf.reduce_sum((tf.abs(x_col-y_lin))**p,axis=2)
    return c

#look into https://github.com/allnightlight/ConditionalWassersteinAutoencoderPoweredBySinkhornDistance/blob/040adb482d4d7aa935939dcdd570122437da099e/wae/wae_trainer.py

def sinkhorn_loss(x,y,epsilon,n,niter,p=2):
    """
    Given two emprical measures with n points each with locations x and y
    outputs an approximation of the OT cost with regularization parameter epsilon
    niter is the max. number of steps in sinkhorn loop
    
    Inputs:
        x,y:  The input sets representing the empirical measures.  Each are a tensor of shape (n,D)
        epsilon:  The entropy weighting factor in the sinkhorn distance, epsilon -> 0 gets closer to the true wasserstein distance
        n:  The number of support points in the empirical measures
        niter:  The number of iterations in the sinkhorn algorithm, more iterations yields a more accurate estimate
    Outputs:
    
    """
    # The Sinkhorn algorithm takes as input three variables :
    C = cost_matrix(x, y,p=p)  # Wasserstein cost function

    # both marginals are fixed with equal weights
    #mu = tf.constant(1.0/n,shape=[n])
    mu = tf.ones([n])
    mu = mu / tf.cast(n, tf.float32)
    nu = tf.ones([n])
    nu = nu / tf.cast(n, tf.float32)
    # Elementary operations
    def M(u,v):
        "Modified cost for logarithmic updates"
        "$M_{ij} = (-c_{ij} + u_i + v_j) / \epsilon$"
        temp1 = (tf.expand_dims(u,1) + tf.expand_dims(v,0) )
        temp2 = -C + temp1
        return temp2 / epsilon
    def lse(A):
        return tf.reduce_logsumexp(A,axis=1,keepdims=True)
    
    # Actual Sinkhorn loop
    u, v = 0. * mu, 0. * nu
    for i in range(niter):
        u = epsilon * (tf.math.log(mu) - tf.squeeze(lse(M(u, v)) )  ) + u
        v = epsilon * (tf.math.log(nu) - tf.squeeze( lse(tf.transpose(M(u, v))) ) ) + v
    
    u_final,v_final = u,v
    pi = tf.exp(M(u_final,v_final))
    cost = tf.reduce_sum(pi*C)
    return cost

def sinkhorn_loss_2(M, m_size, reg, numItermax=1000, stopThr=1e-9):
    #M is cost / M operation above

    a = tf.expand_dims(tf.ones(shape=(m_size[0],)) / m_size[0], axis=1)  # (na, 1)
    b = tf.expand_dims(tf.ones(shape=(m_size[1],)) / m_size[1], axis=1)  # (nb, 1)

     # init data
    Nini = m_size[0]
    Nfin = m_size[1]

    u = tf.expand_dims(tf.ones(Nini) / Nini, axis=1)  # (na, 1)
    v = tf.expand_dims(tf.ones(Nfin) / Nfin, axis=1)  # (nb, 1)

    K = tf.exp(-M / reg)  # (na, nb)

    Kp = (1.0 / a) * K  # (na, 1) * (na, nb) = (na, nb)

    cpt = tf.constant(0)
    err = tf.constant(1.0)

    c = lambda cpt, u, v, err: tf.logical_and(cpt < numItermax, err > stopThr)

    def err_f1():
        # we can speed up the process by checking for the error only all the 10th iterations
        transp = u * (K * tf.squeeze(v))  # (na, 1) * ((na, nb) * (nb,)) = (na, nb)
        err_ = tf.pow(tf.norm(tf.reduce_sum(transp) - b, ord=1), 2)  # (,)
        return err_

    def err_f2():
        return err

    def loop_func(cpt, u, v, err):
        KtransposeU = tf.matmul(tf.transpose(K, (1, 0)), u)  # (nb, na) x (na, 1) = (nb, 1)
        v = tf.div(b, KtransposeU)  # (nb, 1)
        u = 1.0 / tf.matmul(Kp, v)  # (na, 1)

        err = tf.cond(tf.equal(cpt % 10, 0), err_f1, err_f2)

        cpt = tf.add(cpt, 1)
        return cpt, u, v, err

    _, u, v, _ = tf.while_loop(c, loop_func, loop_vars=[cpt, u, v, err])

    result = tf.reduce_sum(u * K * tf.reshape(v, (1, -1)) * M)

    return 

"""
Miscellaneous
"""
class DummyArgParse:
    def __init__(self):
        pass

    def __setarr__(self, key, value):
        self.key = value

    def __getarr__(self, key):
        return self.key
