import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.models import Model
from tensorflow.keras.losses import BinaryCrossentropy, Reduction, MeanSquaredError
from einops.layers.keras import Rearrange as RearrangeEinops

from core.models import VAE
from core.models.utils import _KLDivergenceLayer, TransformerEncoderBlock, _Sampling, _SinkHornLayer
from core.constants import N_CELLS_R, N_CELLS_PHI, N_CELLS_Z, PATCH_R, PATCH_P, PATCH_Z


class PatchEncoder(layers.Layer):
    def __init__(self, num_patches, projection_dim):
        super(PatchEncoder, self).__init__()
        self.num_patches = num_patches
        self.projection = layers.Dense(units=projection_dim)
        self.position_embedding = layers.Embedding(
            input_dim=num_patches, output_dim=projection_dim
        )

    def call(self, patch):
        positions = tf.range(start=0, limit=self.num_patches, delta=1)
        encoded = self.projection(patch) + self.position_embedding(positions)
        return encoded


class TransformerVAE(VAE):
    def _build_encoder(self) -> Model:
        """ Based on a list of intermediate dimensions, activation function and initializers for kernel and bias builds
        the encoder.

        Returns:
             Encoder is returned as a keras.Model.

        """
        with self._strategy.scope():
            # Prepare input layer.
            x_input, e_input, angle_input, geo_input, eps_input = self._prepare_input_layers(for_encoder=True)
            num_patches = PATCH_R * PATCH_P * PATCH_Z
            feature_dim = (N_CELLS_R * N_CELLS_PHI * N_CELLS_Z) // num_patches

            # Patchify and concatenate
            _x_input = layers.Reshape((N_CELLS_R, N_CELLS_PHI, N_CELLS_Z))(x_input)
            patchified = RearrangeEinops("b (i r) (j p) (k z) -> b (i j k) (r p z)", i=PATCH_R, j=PATCH_P, k=PATCH_Z)(_x_input)
            _e_input = layers.Reshape((1, feature_dim))(layers.RepeatVector(feature_dim)(e_input))
            _angle_input = layers.Reshape((1, feature_dim))(layers.RepeatVector(feature_dim)(angle_input))
            _geo_input = layers.Reshape((2, feature_dim))(layers.RepeatVector(feature_dim)(geo_input))
            patches_combined = layers.concatenate([patchified, _e_input, _angle_input, _geo_input], axis=-2)

            # Linear projection and positional embeddings
            encoded_patches = PatchEncoder(num_patches + 4, 256)(patches_combined)

            # Transformer Encoder
            x = TransformerEncoderBlock(encoded_patches, 4, 256, [256,])
            x = layers.Dense(64)(x)
            x = TransformerEncoderBlock(x, 8, 64, [64,])
            x = layers.Dense(16)(x)
            x = TransformerEncoderBlock(x, 16, 16, [16,])

            # Handling transformer representations
            representation = layers.LayerNormalization(epsilon=1e-6)(x)
            representation = layers.Flatten()(representation)
            representation = layers.Dropout(0.2)(representation)

            # Add Dense layer to get description of multidimensional Gaussian distribution in terms of mean
            # and log(variance).
            # z_mean = layers.Dense(self.latent_dim, name="z_mean")(representation)
            # z_log_var = layers.Dense(self.latent_dim, name="z_log_var")(representation)
            # Add KLDivergenceLayer responsible for calculation of KL loss.
            # z_mean, z_log_var = _KLDivergenceLayer()([z_mean, z_log_var])
            # Sample a probe from the distribution.
            # sampling_output = _Sampling()([z_mean, z_log_var, eps_input])
            #sinkhorn loss
            out_sample = layers.Dense(self.latent_dim)(representation)
            bs = tf.shape(out_sample)[0]
            rand_x = tf.random.normal((bs,self.latent_dim),mean=0.0,stddev=1.0) #generate random inout for noise
            cond_x = e_input
            noise = self._generate_noise(rand_x,cond_x,self.latent_dim) #generate noise 
            encoder_output = _SinkHornLayer()(out_sample, self.latent_dim, noise)
            # Create model.
            encoder = Model(inputs=[x_input, e_input, angle_input, geo_input, eps_input], outputs=encoder_output,
                            name="encoder")
            encoder.summary()
        return encoder


    def _build_decoder(self,dense=True) -> Model:

        """ Based on a list of intermediate dimensions, activation function and initializers for kernel and bias builds
        the decoder.

        Returns:
                Decoder is returned as a keras.Model.

        """

        with self._strategy.scope():
            # Prepare input layer.
            latent_input, e_input, angle_input, geo_input = self._prepare_input_layers(for_encoder=False)
            x = layers.concatenate([latent_input, e_input, angle_input, geo_input])
            print(x.shape)
            print("x shape: ", x.shape)

            batch_size = x.shape[0] 
            # z_size = x.shape[1]
            # print("x size: ", z_size)

            #expand dimension
            # x = tf.expand_dims(x, axis = 1)



            # #spatial broadcast
            def spatial_broadcast(x,h=8,w=8,initial=False,number_of_times=2):
                
                # h = h #value corresponding to the first dense layer. in the paper they use the image dimension
                # w = w #value corresponding to the first dense layer. in the paper they use the image dimension
                # b = tf.constant([1,h*w], tf.int32)
                if initial:
                    z_size = x.shape[1]
                    b = tf.constant([1,h], tf.int32) # only increase in one dimension
                    zb = tf.tile(x,b)
                    zb = tf.reshape(zb, [-1, h, z_size])
                    y = tf.linspace(tf.constant(-1, tf.float32), tf.constant(1, tf.float32), h)
                else:
                    z_size = x.shape[2]
                    b = tf.constant([1,h,1], tf.int32) # only increase in one dimension
                    zb = tf.tile(x,b)
                    zb = tf.reshape(zb, [-1, x.shape[1]*h, z_size])
                    y = tf.linspace(tf.constant(-1, tf.float32), tf.constant(1, tf.float32), x.shape[1]*h)


                yb = tf.expand_dims(y, 1)
                yb = tf.expand_dims(yb, 0)
                yb = tf.repeat(yb, tf.shape(zb)[0], axis=0)
                # print("yb shape: ", yb.shape)

                # zsb = tf.concat(values=[zb, xb, yb],axis=-1)
                zsb = tf.concat(values=[zb, yb],axis=-1)
                if number_of_times > 1:
                    for _ in range(1,number_of_times):
                        zsb = tf.concat(values=[zsb, yb],axis=-1) #n time 
                # zsb = tf.concat(values=[zsb, xb, yb],axis=-1) #concat twice to mimic same shape as
                # print("zsb shape: ", zsb.shape)

                return zsb

            #print(x.shape)

            # Transformer Encoder

            if dense:
                zsb = spatial_broadcast(x,h=1,initial=True, number_of_times=2)
                x = layers.Dense(16)(zsb)
                x = TransformerEncoderBlock(x, 16, 16, [16,])
                x = layers.Dense(64)(x)
                x = TransformerEncoderBlock(x, 8, 64, [64,])
                x = layers.Dense(256)(x)
                x = TransformerEncoderBlock(x, 4, 256, [256,])
                x =  layers.Flatten()(x)
                decoder_outputs = layers.Dense(units=self._original_dim, activation=self._out_activation)(x)
            else:
                zsb = spatial_broadcast(x,h=6,initial=True, number_of_times=2)
                x = layers.Dense(16)(zsb)
                print("zsb shape 1: ", zsb.shape)
                x = TransformerEncoderBlock(zsb, 16, 128, [16,])
                x = TransformerEncoderBlock(x, 16, 128, [16,])
                x = TransformerEncoderBlock(x, 16, 128, [16,])
                x = TransformerEncoderBlock(x, 16, 128, [16,])
                zsb = spatial_broadcast(x,h=10, number_of_times=4)
                print("zsb shape 2: ", zsb.shape)
                x = TransformerEncoderBlock(zsb, 8, 32, [20,])
                x = TransformerEncoderBlock(x, 8, 32, [20,])
                x = TransformerEncoderBlock(x, 8, 32, [20,])
                x = TransformerEncoderBlock(x, 8, 32, [20,])
                zsb = spatial_broadcast(x,h=10, number_of_times=5)
                print("zsb shape 3: ", zsb.shape)
                x = TransformerEncoderBlock(zsb, 4, 16, [25,])
                x = TransformerEncoderBlock(x, 4, 16, [25,])
                x = TransformerEncoderBlock(x, 4, 16, [25,])
                x = TransformerEncoderBlock(x, 4, 16, [25,])
                decoder_outputs = tf.keras.activations.sigmoid(x)

            decoder = Model(inputs=[latent_input, e_input, angle_input, geo_input], outputs=decoder_outputs,
                            name="decoder")
            decoder.summary()
        return decoder

    def _generate_noise(self,x,cond_x,latent_dimension) -> Model:
        """ Following Sinkhorn Autoencoder with Noise Generation

        Returns:
                Generated Noise

        """

        with self._strategy.scope():
            # Prepare input layer.
            # latent_input, e_input, angle_input, geo_input = self._prepare_input_layers(for_encoder=False)
            # x = layers.concatenate([latent_input, e_input, angle_input, geo_input])
            # print(x.shape)
            # print("x shape: ", x.shape)

            batch_size = x.shape[0] 

            if cond_x == None:
                x = layers.Dense(32, activation=layers.LeakyReLU(alpha=0.2))(x)
                x = layers.Dense(64, activation=layers.LeakyReLU(alpha=0.2))(x)
                x = layers.Dense(128, activation=layers.LeakyReLU(alpha=0.2))(x)
                # x = layers.Dense(256, activation=layers.LeakyReLU(alpha=0.2))(x)
                # x = layers.Dense(128, activation=layers.LeakyReLU(alpha=0.2))(x)
                x = layers.Dense(64, activation=layers.LeakyReLU(alpha=0.2))(x)
                x = layers.Dense(32, activation=layers.LeakyReLU(alpha=0.2))(x)
                generate_noise = layers.Dense(10)(x)

            else:
                x = layers.Dense(32, activation=layers.LeakyReLU(alpha=0.2))(x)
                x2 = layers.Dense(32, activation=layers.LeakyReLU(alpha=0.2))(cond_x)
                x_concat = tf.concat((x,x2),axis=1)
                x = layers.Dense(64, activation=layers.LeakyReLU(alpha=0.2))(x)
                x = layers.Dense(128, activation=layers.LeakyReLU(alpha=0.2))(x)
                x = layers.Dense(64, activation=layers.LeakyReLU(alpha=0.2))(x)
                x = layers.Dense(32, activation=layers.LeakyReLU(alpha=0.2))(x)
                generate_noise = layers.Dense(10)(x)

            return generate_noise    
