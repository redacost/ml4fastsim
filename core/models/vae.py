import numpy as np
from tensorflow.keras.layers import BatchNormalization, Dense, concatenate, LeakyReLU
from tensorflow.keras.losses import BinaryCrossentropy, Reduction, MeanSquaredError
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input

from core.models import ModelHandler
from utils.optimizer import OptimizerFactory
from core.models.utils import _KLDivergenceLayer, _Sampling, _PhysicsLosses, _SinkHornLayer
from core.constants import LATENT_DIM, ACTIVATION, OUT_ACTIVATION, KERNEL_INITIALIZER, \
    BIAS_INITIALIZER, INTERMEDIATE_DIMS, INLCUDE_PHYSICS_LOSS, LOSS_WEIGHTS, USE_KL, USE_NOISE

import tensorflow as tf


class VAEArch(Model):
    def get_config(self):
        config = super().get_config()
        config["encoder"] = self.encoder
        config["decoder"] = self.decoder
        return config

    def call(self, inputs, training=None, mask=None):
        _, e_input, angle_input, geo_input, _ = inputs
        z = self.encoder(inputs)
        return self.decoder([z, e_input, angle_input, geo_input])

    def __init__(self, encoder, decoder, **kwargs):
        super(VAEArch, self).__init__(**kwargs)
        self.encoder = encoder
        self.decoder = decoder
        self._set_inputs(inputs=self.encoder.inputs, outputs=self(self.encoder.inputs))


def _Loss(y_true, y_pred):
    reconstruction_loss = BinaryCrossentropy(reduction=Reduction.SUM)(y_true, y_pred)
    # reconstruction_loss = MeanSquaredError(reduction=Reduction.SUM)(y_true, y_pred)
    loss = reconstruction_loss
    if INLCUDE_PHYSICS_LOSS:
        loss = LOSS_WEIGHTS[0] * loss
        loss += LOSS_WEIGHTS[1] * _PhysicsLosses(y_true, y_pred)
    return loss


class VAE(ModelHandler):
    def __post_init__(self):
        # self.latent_dim = LATENT_DIM
        # self._intermediate_dims = INTERMEDIATE_DIMS
        # self._activation = ACTIVATION
        # self._out_activation = OUT_ACTIVATION
        # self._kernel_initializer = KERNEL_INITIALIZER
        # self._bias_initializer = BIAS_INITIALIZER
        super().__post_init__()

    def _get_wandb_extra_config(self):
        return {
            "activation": self._activation,
            "out_activation": self._out_activation,
            "intermediate_dims": self._intermediate_dims,
            "latent_dim": self.latent_dim
        }

    def _build_and_compile_new_model(self) -> None:
        # Build encoder and decoder.
        encoder = self._build_encoder()
        decoder = self._build_decoder()

        # Compile model within a distributed strategy.
        with self._strategy.scope():
            # Build VAE.
            self.model = VAEArch(encoder, decoder)
            # Manufacture an optimizer and compile model with.
            optimizer = OptimizerFactory.create_optimizer(self._optimizer_type, self._learning_rate)
            self.model.compile(optimizer=optimizer, loss=_Loss)

    def _build_encoder(self) -> Model:
        """ Based on a list of intermediate dimensions, activation function and initializers for kernel and bias builds
        the encoder.

        Returns:
             Encoder is returned as a keras.Model.

        """

        use_KL = USE_KL

        with self._strategy.scope():
            # Prepare input layer.
            x_input, e_input, angle_input, geo_input, eps_input = self._prepare_input_layers(for_encoder=True)
            x = concatenate([x_input, e_input, angle_input, geo_input])
            # Construct hidden layers (Dense and Batch Normalization).
            for intermediate_dim in self._intermediate_dims:
                x = Dense(units=intermediate_dim, activation=self._activation,
                          kernel_initializer=self._kernel_initializer,
                          bias_initializer=self._bias_initializer)(x)
                x = BatchNormalization()(x)

            if use_KL == True:
                # Add Dense layer to get description of multidimensional Gaussian distribution in terms of mean
                # and log(variance).
                z_mean = Dense(self.latent_dim, name="z_mean")(x)
                z_log_var = Dense(self.latent_dim, name="z_log_var")(x)
                # Add KLDivergenceLayer responsible for calculation of KL loss.
                z_mean, z_log_var = _KLDivergenceLayer()([z_mean, z_log_var])
                # Sample a probe from the distribution.
                encoder_output = _Sampling()([z_mean, z_log_var, eps_input])
                # Create model.
                encoder = Model(inputs=[x_input, e_input, angle_input, geo_input, eps_input], outputs=encoder_output,
                                name="encoder")

            else:
                out_sample = Dense(self.latent_dim)(x)
                if USE_NOISE:
                    bs = tf.shape(out_sample)[0]
                    rand_x = tf.random.normal((bs,self.latent_dim),mean=0.0,stddev=1.0) #generate random inout for noise
                    cond_x = e_input
                    noise = self._generate_noise(rand_x,cond_x,self.latent_dim) #generate noise 
                else:
                    noise=None
                    cond_x=None
                encoder_output = _SinkHornLayer()(out_sample, self.latent_dim, noise, cond_x)

                encoder = Model(inputs=[x_input, e_input, angle_input, geo_input, eps_input], outputs=encoder_output,
                                name="encoder")

        return encoder

    def _build_decoder(self) -> Model:
        """ Based on a list of intermediate dimensions, activation function and initializers for kernel and bias builds
        the decoder.

        Returns:
             Decoder is returned as a keras.Model.

        """

        with self._strategy.scope():
            # Prepare input layer.
            latent_input, e_input, angle_input, geo_input = self._prepare_input_layers(for_encoder=False)
            x = concatenate([latent_input, e_input, angle_input, geo_input])
            # Construct hidden layers (Dense and Batch Normalization).
            for intermediate_dim in reversed(self._intermediate_dims):
                x = Dense(units=intermediate_dim, activation=self._activation,
                          kernel_initializer=self._kernel_initializer,
                          bias_initializer=self._bias_initializer)(x)
                x = BatchNormalization()(x)
            # Add Dense layer to get output which shape is compatible in an input's shape.
            decoder_outputs = Dense(units=self._original_dim, activation=self._out_activation)(x)
            # Create model.
            decoder = Model(inputs=[latent_input, e_input, angle_input, geo_input], outputs=decoder_outputs,
                            name="decoder")
        return decoder

    def _prepare_input_layers(self, for_encoder: bool):
        e_input = Input(shape=(1,))
        angle_input = Input(shape=(1,))
        geo_input = Input(shape=(2,))
        if for_encoder:
            x_input = Input(shape=self._original_dim)
            eps_input = Input(shape=self.latent_dim)
            return [x_input, e_input, angle_input, geo_input, eps_input]
        else:
            x_input = Input(shape=self.latent_dim)
            return [x_input, e_input, angle_input, geo_input]

    def _get_model_specific_data(self, dataset, e_cond, angle_cond, geo_cond):
        noise = np.random.normal(0, 1, size=(dataset.shape[0], self.latent_dim))
        return dataset, e_cond, angle_cond, geo_cond, noise

    def get_decoder(self):
        return self.model.decoder

    def _generate_noise(self,x,cond_x,latent_dimension) -> Model:
        """ Following Sinkhorn Autoencoder with Noise Generation

        Returns:
                Generated Noise

        """

        with self._strategy.scope():
            # Prepare input layer.
            # latent_input, e_input, angle_input, geo_input = self._prepare_input_layers(for_encoder=False)
            # x = layers.concatenate([latent_input, e_input, angle_input, geo_input])
            # print(x.shape)
            # print("x shape: ", x.shape)

            batch_size = x.shape[0] 

            if cond_x == None:
                x = layers.Dense(32, activation=layers.LeakyReLU(alpha=0.2))(x)
                x = layers.Dense(64, activation=layers.LeakyReLU(alpha=0.2))(x)
                x = layers.Dense(128, activation=layers.LeakyReLU(alpha=0.2))(x)
                x = layers.Dense(256, activation=layers.LeakyReLU(alpha=0.2))(x)
                x = layers.Dense(512, activation=layers.LeakyReLU(alpha=0.2))(x)
                x = layers.Dense(128, activation=layers.LeakyReLU(alpha=0.2))(x)
                x = layers.Dense(64, activation=layers.LeakyReLU(alpha=0.2))(x)
                x = layers.Dense(32, activation=layers.LeakyReLU(alpha=0.2))(x)
                generate_noise = layers.Dense(self.latent_dim)(x)

            else:
                x = Dense(32, activation=LeakyReLU(alpha=0.2))(x)
                x2 = Dense(32, activation=LeakyReLU(alpha=0.2))(cond_x)
                x_concat = tf.concat((x,x2),axis=1)
                x = Dense(64, activation=LeakyReLU(alpha=0.2))(x)
                x = Dense(128, activation=LeakyReLU(alpha=0.2))(x)
                x = Dense(64, activation=LeakyReLU(alpha=0.2))(x)
                x = Dense(32, activation=LeakyReLU(alpha=0.2))(x)
                generate_noise = Dense(self.latent_dim)(x)

            return generate_noise    

